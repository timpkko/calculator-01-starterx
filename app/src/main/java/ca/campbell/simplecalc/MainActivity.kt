package ca.campbell.simplecalc

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.util.Log
import android.widget.Toast

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

class MainActivity : Activity() {
    // Get a handle on the views in the UI
    // lateinit allows us to avoid using null initialization & !! & ?
    // but we must be sure to init before use otherwise
    // it acts as if !! was used & crashes on null value
    private lateinit var etNumber1: EditText
    private lateinit var etNumber2: EditText
    private lateinit var result: TextView
    internal var num1: Double = 0.toDouble()
    internal var num2: Double = 0.toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etNumber1 = findViewById(R.id.num1) as EditText
        etNumber2 = findViewById(R.id.num2) as EditText
        result = findViewById(R.id.result) as TextView
        }  //onCreate()

    private fun setNums() {
        Log.d(TAG, "setting numbers")
        try {
            num1 = etNumber1.text.toString().toDouble()
            num2 = etNumber2.text.toString().toDouble()
            Log.d(TAG, etNumber1.text.toString())
            Log.d(TAG, etNumber2.text.toString())
        }
        catch(e: Exception) {
            val error : String = getString(R.string.noValuesError)
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        }
    }

    // TODO: replace with code that adds the two input numbers
    fun addNums(v: View) {
        Log.d(TAG, "adding")
        setNums()
        //  result.text = java.lang.Double.toString(2 * num1)    OK but not kotlin
        result.text = (num1 + num2).toString()

    }  //addNums()

    fun subNums(v: View) {
        Log.d(TAG, "subtracting")
        setNums()
        result.text = (num1 - num2).toString()
    }

    fun mulNums(v: View) {
        Log.d(TAG, "multiplying")
        setNums()
        result.text = (num1 * num2).toString()
    }

    fun divNums(v: View) {
        Log.d(TAG, "dividing")
        if (etNumber2.text.toString() == "0") {
            val divide0 : String = getString(R.string.dividedByZero)
            Toast.makeText(this, divide0, Toast.LENGTH_SHORT).show()
        }
        else {
           setNums()
           result.text = (num1 / num2).toString()

        }
    }

    fun clearEditTexts(v: View) {
        etNumber1.text = null
        etNumber2.text = null
    }

    companion object {
        val TAG = "CALC"
    }

}